﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public List<GameObject> targets;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI gameOverText;
    public Button rButton;
    public GameObject titleScreen;

    private int score;
    private float spawnRate;
    public bool active;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void StartGame(float diff)
    {
        spawnRate = diff;
        active = true;
        StartCoroutine(SpawnTarget());
        score = 0;
        UpdateScore(0);
        titleScreen.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator SpawnTarget()
    {
        while (active)
        {
            yield return new WaitForSeconds(spawnRate);
            int index = Random.Range(0, targets.Count);
            Instantiate(targets[index]);
        }
    }

    public void UpdateScore(int scoreInc)
    {
        score += scoreInc;
        scoreText.text = "Score:\n" + score;
    }

    public void GameOver()
    {
        gameOverText.gameObject.SetActive(true);
        rButton.gameObject.SetActive(true);
        active = false;
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
