﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    private float range = 9.0f;
    public GameObject enemyPrefab;
    public GameObject pUpPrefab;
    public int enemyCount;
    public int waveNum = 1;

    // Start is called before the first frame update
    void Start()
    {
        SpawnEnemyWave(waveNum);
    }

    // Update is called once per frame
    void Update()
    {
        enemyCount = FindObjectsOfType<Enemy>().Length;
        if(enemyCount == 0)
        {
            SpawnEnemyWave(++waveNum);
        }
    }

    void SpawnEnemyWave(int eNum)
    {
        Instantiate(pUpPrefab, GenerateSpawnPos() - new Vector3(0,1.5f,0), enemyPrefab.transform.rotation);
        for (int i = 0; i < eNum; i++)
        {
            Instantiate(enemyPrefab, GenerateSpawnPos(), enemyPrefab.transform.rotation);
        }
    }

    private Vector3 GenerateSpawnPos()
    {
        float spawnPosX = Random.Range(-range, range);
        float spawnPosZ = Random.Range(-range, range);
        Vector3 randomPos = new Vector3(spawnPosX, 1.5f, spawnPosZ);

        return randomPos;
    }
}
